# Please complete the pair_up function below so that it takes a list of items and returns a list of pairs of consecutive items. Here's an example:

# input = [1,2,3,4,5,6,7]

# pair_up(input)  # Returns [[1,2], [3,4], [5,6]]
# Note: since there were an odd number of items, the 7 didn't have a partner to pair with, so it was excluded.

# Think through this problem. It may seem complex, but you can do this.

# What gets returned for an empty list?
# Can you do this with a single if statement?
# Can you do this with a for loop?
# input	output
# []	[]
# [1, 2]	[[1, 2]]
# [1, 2, 3]	[[1, 2]]
# [1, 2, 3, 4]	[[1, 2], [3, 4]]
# [1, 2, 3, 4, 5]	[[1, 2], [3, 4]]


def pair_up(items):
    pass


print(pair_up([]))  # []
print(pair_up([1, 2]))  # [[1, 2]]
print(pair_up([1, 2, 3]))  # [[1, 2]]
print(pair_up([1, 2, 3, 4]))  # [[1, 2], [3, 4]]
print(pair_up([1, 2, 3, 4, 5]))  # [[1, 2], [3, 4]]
