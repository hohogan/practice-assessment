# Implement the function add_all() such that it returns the sum of the numbers in a list.

# Sample inputs/outputs:

# numbers	output
# (none)	0
# [3]	3
# [1, 2, -3]	0

def add_all(numbers):
    pass



print(add_all())  # 0
print(add_all(3))  # 3
print(add_all(1, 2, -3))  # 0
