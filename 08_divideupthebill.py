# Implement the function divide_bill,
# which will return the amount due for each diner given:
# a total bill, number of diners, and the desired tip amount.

# The tip amount is the percentage of the bill to add back to the bill.

# For example, five people go to dinner that costs, in total, $100.
# They decide to leave a 0.25 (25%) tip. That makes the total, including tip, $125.
# They split the bill by dividing 125 by 5 (the number of diners) to get $25 that each must pay.

# bill	num_diners	tip	output
# 10	4	0.0	2.5
# 10	4	0.1	2.75
# 30	5	0.2	7.2

def divide_bill(bill, num_diners, tip):
    pass


print(divide_bill(10, 4, 0.0))   # 2.5
print(divide_bill(10, 4, 0.1))   # 2.75
print(divide_bill(30, 5, 0.2))   # 7.2
