# This pseudo-encryption method works by shifting each letter in a message a fixed amount. So, for example:

# shift_cipher("b", 1)     # --> "c"
# shift_cipher("b", 2)     # --> "d"
# shift_cipher("b", -1)    # --> "a"
# shift_cipher("bbc", 1)   # --> "ccd"
# shift_cipher("bbc", -1)  # --> "aab"
# shift_cipher("bbc", 3)   # --> "eef"
# Can you implement this 2000-year-old cipher?

# You can use the ord built-in method to turn a character into its corresponding number.

# You can use the chr built-in method to turn a number into its corresponding character.

# value = ord("a")    # turns "a" into 97
# value += 1          # adds one to 97 to make it 98
# letter = chr(value) # turns 98 into a letter
# print(letter)       # prints "b"
# Think through this problem. It may seem complex, but you can do this.

# What gets returned for an empty string?
# Can you do this with a single if statement?
# Can you do this with a for loop?
# message	shift	output
# ""	3	""
# "abc"	1	"bcd"
# "Raining-Frogs"	-10	"HW_d_d]#<he]i"

def shift_cipher(message, shift):
    pass


print(shift_cipher("", 3))  # ""
print(shift_cipher("abc", 1))  # "bcd"
print(shift_cipher("Raining-Frogs", -10))  # ""HW_d_d]#<he]i""
