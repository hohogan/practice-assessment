# The join_strings returns a single string that's the concatenation of all of the input strings, separated by the separator. Example:

# join_strings(["aaa", "bbb", "ccc"], "--")
#     # Returns "aaa--bbb--ccc"
# Think through this problem. It may seem complex, but you can do this.

# What gets returned for an empty list?
# Can you do this with a single if statement?
# Can you do this with a for loop?
# strings	separator	expected output
# []	","	""
# ["hello", " world"]	","	"hello, world"
# ["a", "b", "c,d"]	"#"	"a#b#c,d"


def join_strings(strings, separator):
    pass


print(join_strings([], ","))  # ""
print(join_strings(["hello", " world"], ","))  # "hello, world"
print(join_strings(["a", "b", "c,d"], "#"))  # "a#b#c,d"
